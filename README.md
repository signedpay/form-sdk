# SOLID Form SDK

## Разработка

1. Устанавливаем пакеты командой `yarn install` (_рабочая версия ноды `v13.3.0`_)
2. Запускаем сборщика и сервер командой `yarn run encore dev --config-name={name of form}`
3. Сервер запустится по адресу http://localhost:8080

## Сборка
1. Устанавливаем пакеты командой `yarn install`
2. Запускаем сборку командой `yarn run encore production --config-name={name of form | optional} --build-version={version | optional}
`

## Создание нового стиля

1. Создать одноименные директории в `src\base\styles` и `src\templates`
    Например: `src\base\styles\example` и `src\base\templates\example`
2. В созданной директории со стилями создать основной файл стиля `index.styl`
    _Структура компонентов стилей внутри данной директории - на усмотрение разработчиков._
3. В директории формы в `template` создать файл шаблона `index.pug`
    Пример:
    
    ```
    extends ../../../base/templates/layout.pug
    include ../../../base/templates/form
    
    block form
        +form
            +cardNumber
            +expiryDate
            +cardHolder
            +cardCvv
            +cardPin
            +cpf
            +curp
            +dni
            +personalId
            +address
            +state
            +city
            +zipCode

    ```
    
    **Важно: необходимо строго соблюдать порядок очередности подключаемых миксинов элементов (полей) формы, как в примере!**
4. Для просмотра результата запустить сервер `npm run dev` и открыть в браузере URL по имени созданных директорий `http://localhost:8080/example.html`

## Sentry

Для того, чтобы отправлять исключения на сервер необходимо прописать `window.sentryDsn` до подключения main.js. Если переменная `window.sentryDsn` не установлена - то исключения отправлятся на сервер не будут. В dev-сервере это приписывается в `/src/templates/layout.pug` (window.sentryDsn = "...")
Библиотека слушает событие `captureException` на элементе `document`. При получении события `captureException` исключение отправляется в sentry

## Messaging system

У формы, запущенной внутри iframe, есть возможность коммуникации с родительским окном посредством postMessage.

Доступные типы сообщений:

```
WindowHeight {
  height:integer,
  type:"windowHeight"
}
```

```
OrderStatus {
  response: { ... },
  type:"orderStatus"
}
```