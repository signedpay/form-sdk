const formMaster = require('./src/forms/default/webpack.config');

let configs = [formMaster];

module.exports = (env, argv) => {
    if (argv['buildVersion']) {
        let version = argv['buildVersion'];

        for(let config of configs) {
            let name = config.output.filename;
            config.output.filename = name.substring(0, name.length - 3) + '.' + version + '.js';
        }
    }
    return configs;
};