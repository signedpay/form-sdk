const Encore = require('@symfony/webpack-encore');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {resolve} = require('path');

Encore
    .setOutputPath('dist/default/')
    .setPublicPath('/default')
    .addEntry('form-default', './src/forms/default/index.js')
    .addStyleEntry('example', './src/base/styles/example/index.styl')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableStylusLoader()
    .enableSourceMaps(!Encore.isProduction())
    .configureBabel(() => {
    }, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .enableEslintLoader()
    .addPlugin(new UglifyJsPlugin({
        uglifyOptions: {
            comments: false,
            compress: {
                drop_console: true
            },
        }

    }))
    .addPlugin(new HtmlWebpackPlugin({
        template: resolve('src/forms/default/template/index.pug'),
    }))
    .addLoader({
        test : /\.pug$/, use: { loader: 'pug-loader', query: {} }
    })
;

const defaultForm = Encore.getWebpackConfig();
defaultForm.name = 'default';

Encore.reset();

module.exports = defaultForm;