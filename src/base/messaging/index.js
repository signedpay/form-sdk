import MessagingSystem from "./MessagingSystem";
import WindowHeight from "./messageTypes/WindowHeight";
import InfoBlockClick from "./messageTypes/InfoBlockClick";

try {
    const Messaging = new MessagingSystem();
    const ConcreteMessage = new WindowHeight("windowHeight");

    const postHeightMessage = () => {
        const body = document.body,
            html = document.documentElement;

        const height = Math.max(body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight);


        ConcreteMessage.setHeight(height);

        Messaging.sendToDomParent(ConcreteMessage, window);
    };

    const infoBlockClick = (event) => {
        let elem = event.target;
        const message = new InfoBlockClick(elem.dataset.eventType);

        Messaging.sendToDomParent(message, window);
    };

    window.addEventListener('load', postHeightMessage);
    window.addEventListener('resize', postHeightMessage);

    const infoBlockPostMessageElementClass = 'clickable';
    let elements = document.getElementsByClassName(infoBlockPostMessageElementClass);

    Array.from(elements).forEach(function (element) {
        element.addEventListener('click', infoBlockClick);
    });
} catch (err) {
    console.warn('Something went wrong.');
}
