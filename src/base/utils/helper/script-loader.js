export function loadScript(src, callback) {
    let js = document.createElement('script');
    js.src = src;
    js.onload = function() {
         callback();
    };
    js.onerror = function() {
        callback(new Error('Failed to load script ' + src));
    };
    document.head.appendChild(js);
}