import remove from 'lodash/remove';
import {Model} from './Model';
import {initComponent} from './initComponent';
import {COMPONENTS, ELEMENTS} from './symbols';
import i18next from "i18next";

const enLang = require("../../translations/en.json");
const esLang = require("../../translations/es.json");
const frLang = require("../../translations/fr.json");
const ptLang = require("../../translations/pt.json");
const ruLang = require("../../translations/ru.json");
const jaLang = require("../../translations/ja.json");
const itLang = require("../../translations/it.json");
const koLang = require("../../translations/ko.json");
const deLang = require("../../translations/de.json");
const plLang = require("../../translations/pl.json");
const trLang = require("../../translations/tr.json");
const arLang = require("../../translations/ar.json");
const ukLang = require("../../translations/uk.json");

/**
 * Exposes the main public interface to manage application.
 */
export class App {

    constructor(elements) {
        this[COMPONENTS] = {};
        this[ELEMENTS] = new Set();
        this.components = [];
        this.elements = elements;
        this.data = new Model();
        this.data.on('change', this.update.bind(this));
    }

    /**
     * Registers application model with some predefined data
     * @param  {any} data model data
     * @return {App}
     */
    model(data) {
        this.data.reset(data);
        return this;
    }

    /**
     * Registers component constructor bound to selector.
     * @param  {String} selector selector to bind component instance to
     * @param  {Function} ctor   component constructor
     * @return {App}
     */
    component(selector, ctor) {
        this[COMPONENTS][selector] = ctor;
        return this;
    }

    /**
     * Instantiates all registered components within element.
     * @param  {Element} root
     */
    bootstrap(root) {
        let tabIndex = 1;

        Object.keys(this[COMPONENTS]).forEach((selector) => {
            let elements = root.querySelectorAll(selector);
            initComponent(this[COMPONENTS][selector], elements, this);
        });
        this.initTranslation();
    }

    /**
     * Invokes all components' `update` function
     */
    update(element) {
        this.components.forEach(component => component.update && component.update(element));
    }

    /**
     * Registers component instance within application.
     * @param  {Component} component
     */
    register(component) {
        this.components.push(component);
        this[ELEMENTS].add(component.element);
    }

    /**
     * Removes component instance from application
     * @param  {Component} component
     */
    destroy(component) {
        remove(this.components, component);
    }

    initTranslation() {
        i18next.init({
            getAsync: false,
            lng: document.getElementsByTagName('body')[0].getAttribute('lang') || 'en',
            fallbackLng: ['en'],
            debug: false,
            resources: {
                en: {
                    translation: enLang
                },
                ru: {
                    translation: ruLang
                },
                es: {
                    translation: esLang
                },
                fr: {
                    translation: frLang
                },
                pt: {
                    translation: ptLang
                },
                ja: {
                    translation: jaLang
                },
                ko: {
                    translation: koLang
                },
                it: {
                    translation: itLang
                },

                pl: {
                    translation: plLang
                },
                de: {
                    translation: deLang
                },
                tr: {
                    translation: trLang
                },
                ar: {
                    translation: arLang
                },
                uk: {
                    translation: ukLang
                }
            }
        }).then();
    }

}
