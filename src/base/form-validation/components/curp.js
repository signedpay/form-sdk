import {Input} from './input';
import {DEFAULT} from "../error-labels";

export class Curp extends Input {
    isValid() {
        let value = this.model.get(this.full_name);

        if( /[a-zA-Z]{4}[0-9]{6}[a-zA-Z]{6}[a-zA-Z0-9]{1}[0-9]{1}/.test(value)){
            return true;
        }

        this.setValidationErrorToBox(DEFAULT);
        return false;
    }
}