import {App} from '../utils/validation';
import {model} from './model';
import set from 'lodash/set';
import {componentList} from './component-list';
import {FORM_NAME, CHECKSUM_SELECTOR} from './constants';

let form = document.getElementsByTagName('form')[0];
let tabIndex = 1;

let elements = Array.from(form.elements);

let tabIndexQueue = [];

elements.map((item) => {
    if (item.type === "hidden") {
        return;
    }

    let styles = getComputedStyle(item.parentNode.parentElement);

    let order = styles.order;
    let display = styles.display;

    if (display === "none") {
        return;
    }

    if (order != "" && order != 0) {
        order = parseInt(order);
        if (order > tabIndex) {
            tabIndex = order
        }

        tabIndexQueue.push({"item": item, "index": order});
        return;
    }

    tabIndexQueue.push({"item": item, "index": tabIndex});
    tabIndex++;
});

elements = tabIndexQueue.sort(function(a, b) {
    return a.index - b.index;
}).map((elem) => {
    elem.item.setAttribute('tabindex', elem.index);

    return elem.item
});

const app = new App(elements);

const $checksum = document.querySelector(CHECKSUM_SELECTOR);

set(model, `${FORM_NAME}.checkSum`, $checksum.value);

app.model(model);

componentList.forEach(component => {
    if (document.querySelector(component.selector)) {
        app.component(component.selector, component.component);
    }
});

app.bootstrap(document.body);
