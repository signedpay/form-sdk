#!/bin/sh

set -e

aws s3 cp /project/ s3://${BUCKET_NAME}/general/js/ --acl public-read --content-type application/javascript --recursive --exclude '*' --include 'form-*.js' --exclude '*/*'
